--1.1. Base de dades world

/*1. Fes una funció que rebi el nom d’un país i retorni quantes ciutats del país
hi ha.*/

CREATE OR REPLACE FUNCTION ciutatsPais(nomPais text) RETURNS bigint AS $$
    SELECT count(city.id)
    FROM city join country on city.countrycode = country.code
    WHERE (
        SELECT code
        FROM country
        WHERE name like nomPais
    ) like city.countrycode
$$ LANGUAGE SQL;

SELECT ciutatsPais('Afghanistan');

--resultat: 4

/*Fes una funció que rebi el nom d’un idioma i retorni a quants països es parla
aquest idioma.*/

CREATE OR REPLACE FUNCTION idiomaPaisos(nomIdioma text) RETURNS bigint AS $$
    SELECT count(cl.countrycode)
    FROM countrylanguage cl
    WHERE nomIdioma like language
$$ LANGUAGE SQL;

SELECT idiomaPaisos('Spanish');

--resultat: 28

/*Fes una funció que permeti inserir noves ciutats a la base de dades. La
funció rebrà el nom de la ciutat, el codi del país, el districte i la quantitat
d’habitants. Ha de retornar l’id de la ciutat creada (utilitza returning).*/

CREATE OR REPLACE FUNCTION insereix_nova_ciutat(id bigint, nom_ciutat text, 
codi_pais char(3), districte text, num_habitants integer) RETURNS INTEGER AS $$
    INSERT INTO city
    VALUES (id, nom_ciutat, codi_pais, districte, num_habitants)
    RETURNING id
$$ LANGUAGE SQL;

SELECT insereix_nova_ciutat(4500, 'Castellar del Valles', 'ESP', 'Catalunya', 20000);

--4500. No he trobat la manera de fer-ho sense posar un ID

/*Fes una funció que permeti modificar la població d’una ciutat. Per fer-ho no
li donarem la població total, sinó quants habitants nous hi ha i quants
habitants han marxat (per exemple, hi ha 300 habitants nous deguts a naixements
i immigració, i 180 habitants menys deguts a defuncions i emigració). La funció
ha de retornar la quantitat d’habitants actualitzada.*/

CREATE OR REPLACE FUNCTION modifica_poblacio(ciutat text, nous_habitants integer, 
han_marxat integer) RETURNS integer AS $$
    UPDATE city
    SET population = population + (nous_habitants - han_marxat)
    WHERE name like ciutat
    RETURNING population
$$ LANGUAGE SQL;

SELECT modifica_poblacio('Kabul', 300, 100);

--Inicialment Kabul 1780000
--Després 1780200

--1.2. Base de dades hotel

/*Fes una funció que rebi una data i retorni la quantitat d’hostes que hi
havia a l’hotel en aquesta data.*/

CREATE OR REPLACE FUNCTION quantitat_hostes(data date) RETURNS bigint AS $$
    SELECT count(h.id)
    FROM hosts h join stayhosts sh on h.id = sh.hostid
    JOIN stays s on sh.stayid = s.id
    WHERE data >= checkin AND data <= checkout
$$ LANGUAGE SQL;

SELECT quantitat_hostes('2018-02-14');

--14

/*Fes una funció que rebi un tipus i número de document i ens retorni a quina
habitació està l’hoste que té aquest document (o NULL si en aquests moments
no es troba a l’hotel).*/

CREATE OR REPLACE FUNCTION quina_habitacio(tipus varchar, numero varchar) 
RETURNS bigint AS $$
    SELECT s.roomnumber
    FROM hosts h JOIN stayhosts sh on h.id = sh.hostid
    JOIN stays s on sh.stayid = s.id
    WHERE h.doctype like tipus AND h.docnumber like numero
$$ LANGUAGE SQL;

SELECT quina_habitacio('Passport', '69410682');

--Resultat 40.

/*Fes una funció que rebi l’id d’un tipus d’habitació, una data d’entrada i
una data de sortida, i ens retorni un número d’habitació que estigui lliure
del tipus demanat en aquestes dates. Si n’hi ha moltes, fes que retorni la que
tingui un número més baix. Si no hi ha cap habitació disponible, la funció ha
de retornar NULL.*/

CREATE OR REPLACE FUNCTION habitacio_disp(tipus_hbtc bigint, entrada date, 
sortida date) RETURNS bigint AS $$
    SELECT s.roomnumber
    FROM rooms r RIGHT JOIN stays s on r.roomnumber = s.roomnumber
    WHERE r.roomtypeid = tipus_hbtc AND entrada = checkin AND sortida = checkout
    ORDER BY s.roomnumber ASC
    LIMIT 1
$$ LANGUAGE SQL;

SELECT habitacio_disp(5, '2018-08-20', '2018-08-24');

-- Em sortira de resultat: 7

/*Fes una funció que rebi el nom d’una temporada, el nom d’un tipus d’habitació
i un preu, i assigni el preu que rep com a preu del tipus d’habitació
especificat a la temporada especificada. La funció retornarà el preu assignat.*/

CREATE OR REPLACE FUNCTION assigna_preu(nom_temporada varchar, tipus_hbtc varchar, 
preu numeric) RETURNS numeric AS $$
    UPDATE priceseasons
    SET price = preu
    FROM seasons s JOIN priceseasons ps on s.id = ps.seasonid
    JOIN roomtypes r on ps.roomtypeid = r.id
    WHERE ps.roomtypeid = ( 
        SELECT id
        FROM roomtypes
        WHERE name like tipus_hbtc
    )
    AND ps.seasonid = (
        SELECT id
        FROM seasons
        WHERE name like nom_temporada
    )
    RETURNING preu
$$ LANGUAGE SQL;

SELECT assigna_preu('Autumn 2020', 1, 30);

-- Abans 31.68, ara 50 --NO FUNCIONA, ES CANVIA TOTA LA COLUMNA

/*Fes una funció que rebi el nom d’un tipus d’habitació i el nom d’una
característica, i retorni si el tipus d’habitació té o no la característica
en qüestió.*/

CREATE OR REPLACE FUNCTION te_caracteristica(nom_hbtc varchar, caracteristica varchar) 
RETURNS varchar AS $$
    SELECT f.name
    FROM hotel.roomtypes rt join hotel.roomtypefacilities rf on rt.id = rf.roomtypeid
    JOIN hotel.facilities f on rf.facilityid = f.id
    WHERE rt.name like nom_hbtc AND f.name like caracteristica
$$ LANGUAGE SQL;

SELECT te_caracteristica('Single room with shared bathroom', 'Telephone');

--NULL PERQUE NO TE AQUESTA CARACTERISTICA

SELECT te_caracteristica('Single room with shared bathroom', 'WIFI');

--SURT WIFI PERQUE SI LA TE

/*Fes una funció que rebi un tipus i número de document i ens retorni la
quantitat de nits que l’hoste amb aquest document ha passat a l’hotel. Ha de
retornar 0 si l’hoste no existeix a la base de dades.*/

CREATE OR REPLACE FUNCTION quantitat_nits(tip_doc varchar, num_doc varchar) 
RETURNS int AS $$
    SELECT s.checkout::date - s.checkin::date
    FROM hosts h join stayhosts sh on h.id = sh.hostid
    JOIN stays s on sh.stayid = s.id
    WHERE h.docnumber like num_doc AND h.doctype like tip_doc
$$ LANGUAGE SQL;

SELECT quantitat_nits('Passport', '69410682');

--5