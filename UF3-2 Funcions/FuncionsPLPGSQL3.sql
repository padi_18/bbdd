-- Fes la funció search_film que rebi com a paràmetre un text i retorni un
-- conjunt de pel·lícules. La funció comprovarà si l’argument conté algun símbol %.
-- Si no en conté n’afegirà un al principi i un al final. Llavors, cercarà totes
-- les pel·lícules el títol de les quals coincideixi amb el text rebut.

create function search_film(titol text) returns setof varchar as $$
declare
    pelis integer;
begin
    select position('%' in titol) into strict pelis;

    if pelis = 0 then
        titol := concat('%', titol, '%');
    end if;

    return query
        select title
        from film
        where title like titol;
end;
$$ language plpgsql;

select search_film('AIRPLANE'); 

-- %NENE%

-- Fes una versió millorada de la funció anterior, search_film, que rebi
-- dos textos. El segon argument podrà ser "title", "description" o "genre". La
-- cerca es farà com a l’exercici anterior, però pel camp especificat. Si el
-- camp és incorrecte, cal llançar una excepció.

create function search_film2(titol text, opcio text) returns setof varchar as $$ 
declare
    pelis int;
    textMayus text;
begin
    select position('%' in titol) into strict pelis;

    if pelis = 0 then
        titol := concat('%', titol, '%');
    end if;

    select upper(opcio) into strict textMayus;

    case
        when textMayus like 'TITLE' then
            return query
                select title
                from film
                where title like titol;
        when textMayus like 'DESCRIPTION' then 
            return query
                select title
                from film
                where description like titol;
        when textMayus like 'GENRE' then
            return query
                select f.title
                from film f join film_category fc on f.film_id = fc.film_id 
                join category c on fc.category_id = c.category_id 
                where c.name like titol;
    end case;
exception
    when no_data_found then
        raise exception 'No hi ha resultats amb la cerca';
    when too_many_rows then
        raise exception 'Retorna més de una fila';
    when others then
        raise exception 'Surten diferents errors';
end;
$$ language plpgsql;

select search_film2('AFRICAN', 'titlE'); -- AIRPLANE SIERRA, RAGING AIRPLANE
select search_film2('Epic', 'DescripTion'); -- surten 42 linias
select search_film2('Action', 'GENre'); -- surten 64 linias

-- Fes una funció que rebi el títol d’una pel·lícula i un codi de botiga i
-- retorni els ítems d’inventari corresponents a aquesta pel·lícula que estiguin a
-- la botiga indicada i que, a més, no estiguin en lloguer en aquests moments.

create function peliDisponible(titol text, codiBotiga int) returns setof inventory as $$ 
declare
    pelis int;
    titolMayus text;
    bonTitol text;
begin
    select upper(titol) into strict titolMayus; 

    select position('%' in titolMayus) into strict pelis;
    
    if pelis = 0 then
        bonTitol:= concat('%', titolMayus, '%');
    end if;

    return query
        select i.*
        from inventory i join rental r on i.inventory_id = r.inventory_id 
        join film f on i.film_id = f.film_id 
        where f.title like bonTitol 
        and i.store_id = codiBotiga 
        and r.return_date is not null;
end;
$$ language plpgsql;

select peliDisponible('AIRPLANE', 2); 

-- surten 22 files

-- Crea una funció que mostri el nom complet, el telèfon i el títol de la
-- pel·lícula de tots aquells clients que tenen un títol que ja haurien d’haver
-- retornat. La funció retornarà un conjunt amb tots aquests clients.

create function peliculaPerRetornar() returns setof text as $$ 
declare
    titolPelicula text;
begin
   return query
        select concat(
            'Nom i cognom: ', 
            c.first_name, ' ', c.last_name, 
            ' / Telèfon: ', a.phone, 
            ' / Titol: ', f.title
        )
        from customer c
        join address a on c.address_id = a.address_id 
        join rental r on c.customer_id = r.customer_id 
        join inventory i on r.inventory_id = i.inventory_id 
        join film f on i.film_id = f.film_id 
        where r.return_date is null;
end;
$$ language plpgsql;

select peliculaPerRetornar();

-- 183 files

-- Fes una funció que permeti retornar una pel·lícula. La funció rebrà
-- l’identificador del client i l’identificador de l’ítem a retornar. Cal que es
-- llanci una excepció en cas que el client no tingui en lloguer l’ítem
-- especificat. El text de l’excepció ha d’indicar si l’ítem no està llogat o, si
-- ho està, ha d’indicar quin client el té en lloguer.

create function retornarPeli(id_client int, id_pelicula int) returns text as $$
declare
    llogada text;
    tePeli int;
    clientQueTePeli int;
begin

    select return_date into strict llogada
    from rental
    where inventory_id = id_pelicula
    order by return_date desc limit 1;

    if llogada is null then
        select customer_id into strict clientQueTePeli
        from rental
        where inventory_id = id_pelicula and return_date is null;
        
        if clientQueTePeli != id_client then 
            return concat('Aquesta peli la te llogada el client ', clientQueTePeli);
            else 
                update rental 
                set return_date = timestamp 'now' 
                where rental_id = tePeli;
            return 'Tornada'; 
        end if;
    else 
        return 'La pel·licula està disponible.';
    end if;
end;
$$ language plpgsql;

drop function retornarPeli(int, int);

select retornarPeli(200, 20); -- Tornada -- el rental id: 14098 -- return date 2020-05-26 17:34:12
select retornarPeli(200, 6); -- Aquesta peli la te llogada el client 208
select retornarPeli(200, 678); -- La pel·licula està disponible.