-- 1.1. Base de dades WORLD



-- Fes una funció que rebi una ciutat (no el nom, sinó una fila sencera) i que
-- retorni quin tant per cent representa la població d’aquesta ciutat respecte a
-- la població general del país.

CREATE OR REPLACE FUNCTION tant_per_cent_poblacio(c city) RETURNS int AS $$
    SELECT ((100 * c.population) / co.population) AS percentatge
    FROM country co
    WHERE c.countrycode = co.code
$$ LANGUAGE SQL;

select tant_per_cent_poblacio(c.*)
from city c
where c.name like 'Kabul'

--1.	7

-- Fes una funció que rebi el codi d’un país i retorni la fila sencera
-- corresponent de la taula.

CREATE OR REPLACE FUNCTION codi_pais(codi char) RETURNS country AS $$
    SELECT *
    FROM country
    WHERE code like codi
$$ LANGUAGE SQL;

select codi_pais('AFG');

1	(AFG,Afghanistan,Asia,"Southern and Central Asia",652090,1919,22720000,45.9,
5976.00,,Afganistan/Afqanestan,"Islamic Emirate","Mohammad Omar",1,AF)

-- Fes una funció que rebi el codi d’un país i retorni dos valors: el nom del
-- país en anglès i el nom del país en l’idioma local.

CREATE OR REPLACE FUNCTION idioma_nom_pais(codi char, OUT nom_angles text, 
OUT nom_local text) AS $$
    SELECT name, localname
    FROM country 
    WHERE codi like code 
$$ LANGUAGE SQL;

select idioma_nom_pais('NLD')

-- Resultat:
-- idioma_nom_pais
-- record
-- 1	(Netherlands,Nederland)

o 

select * from idioma_nom_pais('NLD')

--      nom_angles  |  nom_local
--      text           text
-- 1	Netherlands	   Nederland

-- Fes una funció que rebi el nom d’un idioma i retorni el nom de tots els
-- països on es parla aquest idioma.

CREATE OR REPLACE FUNCTION idioma_paisos(idioma text) RETURNS setof text AS $$
    SELECT c.name
    FROM country c join countrylanguage cl on c.code = cl.countrycode
    WHERE idioma like cl.language
$$ LANGUAGE SQL;

select idioma_paisos('Spanish')

	
-- idioma_paisos
-- text
-- 1	Andorra
-- 2	Argentina
-- 3	Aruba
-- 4	Belize
-- 5	Bolivia
-- 6	Chile
-- 7	Costa Rica
-- 8	Dominican Republic
-- 9	Ecuador
-- 10	El Salvador
-- 11	Spain
-- 12	Guatemala
-- 13	Honduras
-- 14	Canada
-- 15	Colombia
-- 16	Cuba
-- 17	Mexico
-- 18	Nicaragua
-- 19	Panama
-- 20	Paraguay
-- 21	Peru
-- 22	Puerto Rico
-- 23	France
-- 24	Sweden
-- 25	Uruguay
-- 26	Venezuela
-- 27	United States
-- 28	Virgin Islands, U.S.

-- Sense setof nomes retornaria Andorra.



-- 1.2. Base de dades HOTEL



-- Fes una funció que rebi un tipus d’habitació i una data i retorni el conjunt
-- de totes les habitacions disponibles del tipus especificat en aquesta data.

CREATE OR REPLACE FUNCTION habitacions_disponibles(tipus_habitacio varchar, 
data_habitacio date) RETURNS setof bigint AS $$
    SELECT r.roomnumber
    FROM rooms r join roomtypes rt on r.roomtypeid = rt.id
    join bookings b on rt.id = b.roomtypeid
    WHERE r.empty = true 
    and tipus_habitacio like rt.name
    and data_habitacio > b.checkin and data_habitacio < b.checkout
$$ LANGUAGE SQL;

select habitacions_disponibles('Triple room with shared bathroom', '2018-03-10')

-- habitacions_disponibles
-- bigint
-- 1.	4
-- 2.	8
-- 3.	9

-- Fes una funció que rebi un número d’habitació i una data i retorni el
-- conjunt d’hostes que ocupaven l’habitació en aquesta data.

CREATE OR REPLACE FUNCTION hostes_habitacio(num_habitacio bigint, data_habitacio 
date) RETURNS setof varchar AS $$
    SELECT h.firstname
    FROM stays s join stayhosts sh on s.id = sh.stayid
    join hosts h on sh.hostid = h.id
    WHERE num_habitacio = s.roomnumber
    and data_habitacio < s.checkout and data_habitacio > checkin
$$ LANGUAGE SQL;

select hostes_habitacio(28, '2018-01-28')

-- hostes_habitacio
-- character varying
-- 1	RAFAEL
-- 2	VALERE

-- Fes una funció que rebi una data i retorni la quantitat d’hostes que hi havia
-- a l’hotel en aquesta data, la quantitat de diners que hem cobrat en aquesta
-- data, i la quantitat de reserves que s’han fet en aquesta data.

CREATE OR REPLACE FUNCTION hostes_diners_reserves(fecha date, OUT numHostes 
bigint, OUT quantitatDiners numeric, OUT reserves bigint) AS $$
    SELECT count(h.id), sum(s.totalprice), count(s.bookingid)
    FROM hosts h join stayhosts sh on h.id = sh.hostid
    join stays s on sh.stayid = s.id
    WHERE fecha > s.checkin and fecha < s.checkout
$$ LANGUAGE SQL;

select * from hostes_diners_reserves('2018-03-30')

	
    -- numhostes  |  quantitatdiners  |  reserves
    --   bigint            numeric        bigint

-- 1	    9	         3708.32	         9
