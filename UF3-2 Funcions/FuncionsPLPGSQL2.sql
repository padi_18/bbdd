-- Fes una funció que rebi com a paràmetre un codi de país i retorni la seva
-- població i esperança de vida. S’han de tractar els errors "no hi ha resultats",
-- "la consulta retorna més d’una fila" i qualsevol altre.

create function poblacio_vida(codiPais char) returns text as $$ 
declare 
    resultat text;
begin
    select concat('Poblacio: ', population, ' / Esperança de vida: ', lifeexpectancy)
    into strict resultat
    from country
    where code like codiPais;
    return resultat;
exception
    when no_data_found then
        raise exception 'No hi ha resultats amb la cerca --> %', codiPais;
    when too_many_rows then
        raise exception 'Amb % retorna més de una fila', codiPais;
    when others then
        raise exception 'Amb % surten diferents errors', codiPais;
end;
$$ language plpgsql;

select poblacio_vida('NLD'); --=Poblacion: 15864000 / Esperanza de vida: 78.3000031
select poblacio_vida('N-D'); --=error "No hay resultados con la busqueda N-D"

-- Fes una funció que rebi com a paràmetre el nom d’un districte i retorni el
-- nom del país on és. S’han de comprovar els mateixos errors que a l’exercici
-- anterior.

create or replace function districte_pais(districte text) returns text as $$
declare
    resultat text;
begin
    select c.name
    into strict resultat
    from country c join city ci on c.capital = ci.id
    where ci.district like districte;
    return resultat;
exception
    when no_data_found then
        raise exception 'No hi ha resultats amb la cerca --> %', districte;
    when too_many_rows then
        raise exception 'Amb % retorna més de una fila', districte;
    when others then
        raise exception 'Amb % surten diferents errors', districte;
end;
$$ language plpgsql;

select districte_pais('Kabol'); --Afghanistan
select districte_pais('JGF'); --No hi ha resultats amb la cerca --> %

-- Fes una funció que rebi com a paràmetres el codi d’un país i un nou valor
-- per a la població. La funció comprovarà si el país existeix i si la nova
-- població no implica una variació de més del 20% de la població anterior. Si les
-- dues condicions es compleixen, la funció actualitzarà la població del país. En
-- cas contrari, registrarà l’error que s’ha produït.

create or replace function actualitza_poblacio(codiPais char, nouValor int) 
returns text as $$
declare
    poblacioAnterior integer;
begin
    select c.population into strict poblacioAnterior
    from country c 
    where c.code like codiPais;

    if poblacioAnterior is not null and nouValor < ((20 * poblacioAnterior) / 100 + 
    poblacioAnterior) then
        update country
        set population = nouValor
        where code like codiPais;
        return 'Inserció vàlida!';
    else
        return 'No ha sigut exitós';
    end if;
exception
    when no_data_found then
        raise exception 'No hi ha resultats amb la cerca';
    when too_many_rows then
        raise exception 'Retorna més de una fila';
    when others then
        raise exception 'Surten diferents errors';
end;
$$ language plpgsql;

select actualitza_poblacio('ESP', 100000000);

-- Fes una funció que creï cinc noves ciutats a Espanya. Els seus id seran
-- consecutius a partir de l’últim que hi hagi a la taula, el nom de totes les
-- ciutats serà Nova ciutat, estaran al districte Nou districte, i tindran
-- una població de 10000 habitants. La funció mostrarà per pantalla un missatge
-- amb la quantitat de ciutats espanyoles abans i després de la modificació.

create function noves_ciutats() returns text as $$
declare
    ciutatVella text;
    ciutatNova text;
    IDultima int;
begin
    select count(id) into strict ciutatVella
    from city
    where countrycode like 'ESP';
    
    select id into strict IDultima
    from city
    order by id desc limit 1;

    for i in 1..5 loop
      insert into city (id, name, countrycode, district, population)
      values (IDultima + i, 'Nova Ciutat', 'ESP', 'Nou districte', 10000);
    end loop;

    select count(id) into strict ciutatNova
      from city
     where countrycode like 'ESP';

    return concat('Ciutats Anteriors: ', ciutatVella, '/ Ciutats Actuals: ', 
    ciutatNova);
end;
$$ language plpgsql;

select noves_ciutats();

-- Crea una funció que elimini les ciutats creades a l’exercici anterior i que
-- retorni la quantitat de ciutats que s’han eliminat.

create or replace function eliminar_ciutats() returns text as $$
declare
    IDVella integer;
    IDNova integer;
    IDTotal integer;
begin
    select count(id) into strict IDVella
    from city
    where countrycode like 'ESP';

    delete from city
    where name like 'Nova Ciutat' and district like 'Nou districte';

    select count(id) into strict IDNova
    from city
    where countrycode like 'ESP';

    select IDNova - IDVella into strict IDTotal;

    return concat('Han sigut esborrades: ', IDTotal, ' ciutats.');
end;
$$ language plpgsql;

select eliminar_ciutats();

-- Fes una funció que rebi el nom d’un idioma, el codi d’un país, i el tant
-- per cent dels seus parlants. La funció afegirà l’idioma al país en el tant
-- per cent indicat, com a idioma no oficial. La funció comprovarà que el país i
-- l’idioma ja existeixen a la base de dades, que el tant per cent indicat és
-- superior a 0, que l’idioma no està relacionat encara amb el país indicat,
-- i que el total de sumar el tant per cent de tots els idiomes del país no
-- supera el 100%. En cas que hi hagi algun d’aquests errors, no es modificarà la
-- base de dades, i s’informarà amb un missatge.

create or replace function introduir_idioma(nomIdioma text, codiPais char,
percentatgeParlants float) returns text as $$
declare
    comprovacio boolean;
    percentatgeTotal float;
begin
    select exists(
        select * 
        from countrylanguage 
        where countrycode like codiPais and language like nomIdioma
    ) into strict comprovacio;

    select sum(percentage) into strict percentatgeTotal
    from countrylanguage
    where countrycode like codiPais;

    if nomIdioma is not null and codiPais is not null and percentatgeParlants > 0 
    and comprovacio = false and percentatgeTotal < 100 then
        insert into countrylanguage
        values (codiPais, nomIdioma, 'false', percentatgeParlants);
        return 'Inserció Vàlida';
    else
        return 'Alguna cosa és errònea!';
    end if;
end;
$$ language plpgsql;

select introduir_idioma('English', 'ESP', 0.01); -- Inserció Vàlida

-- Crea una funció que rebi un idioma i un codi de país i esborri la seva relació. 
-- Ha de retornar la quantitat d’idiomes que es parlen al país després de la 
-- modificació. Cal comprovar els errors habituals: que no existeixi el país o
-- l’idioma, o que no estiguin relacionats.

create or replace function esborra_relacio_idioma(idioma text, codiPais char)
returns int as $$
declare
    totalIdiomes integer;
    comprovacio boolean;
begin
    select exists(
        select * 
        from countrylanguage 
        where countrycode like codiPais and language like idioma
    ) into strict comprovacio;

    if idioma is not null and codiPais is not null and comprovacio = true then
        delete from countrylanguage
        where language like idioma and countrycode like codiPais;
    end if;

    select count(countrycode) into strict totalIdiomes
    from countrylanguage
    where countrycode like codiPais;

    return totalIdiomes;
end;
$$ language plpgsql;

drop function esborra_relacio_idioma(text, char);

select esborra_relacio_idioma('Spanish', 'ESP'); -- 4
