-- BBDD world
-- 1. Fes una funció que rebi com a paràmetres dos nombres, i retorni la seva suma.

CREATE FUNCTION suma_nombres(num1 int, num2 int) RETURNS int AS $$
    BEGIN
        RETURN num1 + num2;
    END;
$$ LANGUAGE PLPGSQL;

select suma_nombres(2, 2);

-- 4

-- 2. Fes una funció que rebi com a paràmetre dos cadenes de text i mostri un
-- missatge indicant la longitud de cadascuna d’elles. La funció retornarà la
-- longitud de les dues cadenes sumada.

CREATE FUNCTION longitud_cadena(cadena1 text, cadena2 text) RETURNS text AS $$
    BEGIN
        return concat('Longitud cadena 1: ', length(cadena1), ' - Longitud cadena 2: ', 
        length(cadena2), ' / Total: ', length(concat(cadena1, cadena2)));
    END;
$$ LANGUAGE PLPGSQL;

select longitud_cadena('xavi', 'padilla');

-- Longitud cadena 1: 4 - Longitud cadena 2: 7 / Total: 11

-- 3. Fes una funció que rebi el nom d’un idioma i retorni a quants països es parla
-- aquest idioma. Aquesta funció ja l’hem feta en versió SQL, però ara la volem
-- amb PL/pgSQL.

CREATE FUNCTION idioma_paisos(idioma text) RETURNS setof text AS $$
    BEGIN
        return query   
        SELECT country.name 
        FROM countrylanguage c 
        join country on country.code = c.countrycode 
        WHERE c."language" like idioma;
    END;
$$ LANGUAGE PLPGSQL;

select idioma_paisos('Spanish');

-- resultat com l'exercici de la pràctica anterior.

-- 4. Fes una funció que permeti inserir noves ciutats a la base de dades. La
-- funció rebrà el nom de la ciutat, el codi del país, el districte i la quantitat
-- d’habitants. Ha de retornar l’id de la ciutat creada. Aquesta funció ja l’hem
-- feta en versió SQL, però ara la volem amb PL/pgSQL.

CREATE FUNCTION ciutat_nova(nom text, codiPais char, districte text, num_habitants int) 
RETURNS setof int AS $$
    BEGIN
        INSERT INTO city (id, name, countrycode, district, population)
        VALUES ((select id + 1 from city order by id desc limit 1), nom, codiPais, 
        districte, num_habitants);

        return query 
        select id 
        from city 
        order by id desc 
        limit 1;
    END;
$$ LANGUAGE PLPGSQL;

select ciutat_nova('Kenia', 'KNA', 'Mongolia', 299999);

-- 4081

-- 5. Fes una funció que retorni la quantitat total de files que hi ha entre totes
-- les taules de la base de dades.

CREATE FUNCTION quantitat_files() RETURNS bigint AS  $$
    DECLARE 
        FilesCiutat bigint;
        FilesCountry bigint;
        FilesLanguage bigint;
    BEGIN
        SELECT count(*) into FilesCiutat FROM "city";
        SELECT count(*) into FilesCountry FROM "country";
        SELECT count(*) into FilesLanguage FROM "countrylanguage";
        RETURN FilesCiutat + FilesCountry + FilesLanguage;
    END;
$$ LANGUAGE PLPGSQL;

select quantitat_files(); 

--5304

-- 6. Fes una funció que rebi el codi d’un país, i retorni la ciutat més poblada
-- del país. Fes que es produeixi un error si el codi del país no existeix (o es
-- tracta d’un país sense cap ciutat).

CREATE FUNCTION poblacioMaxima(CodiPais char) RETURNS setof text AS $$
    BEGIN
        return query
            select concat(city.name, ': ', city.population)
            from city
            where city.countrycode like CodiPais
            order by city.population desc limit 1;
        if not found THEN
            RAISE EXCEPTION 'No hi ha cap pais amb el següent codi --> %', CodiPais;
        end if;
    END;
$$ LANGUAGE PLPGSQL;

select poblacioMaxima('NLD'); --=Amsterdam: 731200
select poblacioMaxima('LND');

-- 7. Fes una funció que retorni la quantitat de files que hi ha en cada taula de
-- la base de dades, en tres paràmetres de sortida.

CREATE FUNCTION num_files(OUT ciutat bigint, out pais bigint, out llengua bigint) 
AS $$
    BEGIN
        select count(*) into ciutat from city;
        select count(*) into pais from country;
        select count(*) into llengua from countrylanguage;
    END;
$$ LANGUAGE PLPGSQL;

select * from num_files();
--4081      -       239      -       984

-- 8. Fes una funció que rebi el nom d’un idioma i retorni tots els països on es
-- parla aquest idioma. Utilitza RETURN QUERY. A més, la funció mostrarà per
-- pantalla a quants països es parla l’idioma en qüestió.

create function idiomes(idioma text) RETURNS setof text AS $$
    BEGIN
        RETURN QUERY 
        select c.name 
        from countrylanguage cl
        join country c on c.code = cl.countrycode 
        where cl.language like idioma;
    END;
$$ LANGUAGE PLPGSQL;

select idiomes('Spanish');