/*1.Crea un usuari “terrassa” sense contrasenya i no li assignis cap privilegi. 
Prova de fer la connexió a la BD hotel des del dbeaver amb aquest usuari. Pots?*/
createuser terrassa;
-- No es pot iniciar la sessió per que em demana contrasenya.

/*2.	Crea un usuari “sabadell” amb contransenya «super3» i ara prova de fer la connexió 
a la BD hotel amb el Dbeaver. Pots? Justifica la resposta.*/

-- dintre de psql:

create user sabadell password 'super3';

-- Ara si que funciona!! Amb contrasenya.

/*3.Prova d’obrir la taula customers de la BD hotel des del Dbeaver amb aquesta connexió. 
Et deixa? Justifica la resposta.*/

-- No es pot ja que no tenim permisos.

/*4.Quins privilegis tenen per defecte aquests usuaris que has creat abans? 
Consulta els apunts.*/

-- Poden Heredar i poden fer login;

/*5.Ara des de la línia d'ordres, llista els usuaris que hi ha i mira els privilegis 
que tenen cadascun d’ells. */

\du

/*6.Prova 	de connectar-te amb la comanda psql -h 127.0.0.1 -U terrassa -d hotel. 
Explica cada opció.*/

-- el -h és el host al que ens connectem.
-- el -U és l'usuari que utilitzarem.
-- el -d és la base de dades a connectar-nos.

--No podem connectarnos ja que no tenim contrasenya.

--7.Connectat ara a la BD hotel amb l’usuari admin.

psql -U admin -h 192.168.50.101 -d hotel;

/*8.Concedeix a l'usuari “sabadell” el privilegi de consultar la taula customers 
i prova que funciona amb alguna consulta.*/

grant usage on schema hotel to sabadell;
grant select on customers to sabadell;

--9.Consulta ara els privilegis de cada taula de la BD hotel.

\dp

--10.Explica els privilegis de la taula customers.

-- Tenen acces a la taula customers l'usuari admin i l'usuari sabadell

/*11.Concedeix a l’usuari “sabadell” el privilegi de fer update i insert a la taula 
bookings. Comprova que funciona fent algun insert.*/

grant update, insert on bookings to sabadell;

-- Si es pot

--12.Consulta de nou els privilegis de cada taula.

\dp

-- Ara tenim en la taula bookings també amb privilegis de sabadell i admin una altre vegada.

/*13.Dona 	el privilegi de poder consultar totes les taules de la BD hotel a 
l’usuari “sabadell” . Comprova que funciona fent algun select a alguna taula.*/

grant select on all tables in schema hotel to sabadell;

/*14.Ara prova de donar-li el privilegi de consultar la taula productes a l’usuari 
“sabadell” sobre la BD miniwind. Comprova els privilegis.*/

\c miniwind;

grant usage on schema miniwind to sabadell;

--15.Esborra tots els usuaris creats.

dropuser sabadell
dropuser terrassa