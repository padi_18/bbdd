--Creació de rols
 CREATE ROLE salesrepresentative VALID UNTIL '12-31-2020';
 CREATE ROLE salesmanager;
 CREATE ROLE salescoordinator;

--Permisos
GRANT SELECT ON customers, orders TO salesrepresentative;
GRANT SELECT ON customers, orders, products TO salesmanager;
GRANT SELECT, UPDATE, INSERT, DELETE ON ALL TABLES IN SCHEMA miniwind TO salescoordinator;
REVOKE SELECT, UPDATE, INSERT, DELETE ON employees FROM salescoordinator;
GRANT CREATE ON SCHEMA miniwind TO salescoordinator;

--Creació usuaris
CREATE USER jur1 WITH PASSWORD 'sabadell'; --Representative
CREATE USER poi3 WITH PASSWORD 'sabadell'; --Representative
CREATE USER cop5 WITH PASSWORD 'sabadell'; --Manager
CREATE USER jir8 WITH PASSWORD 'sabadell'; --Coordinator

--Fiquem els usuaris als rols
GRANT salesrepresentative TO jur1, poi3;
GRANT salesmanager TO cop5;
GRANT salescoordinator TO jir8;

--Creació usuari administrador
CREATE USER adminsales;
GRANT ALL PRIVILEGES ON DATABASE miniwind TO adminsales;