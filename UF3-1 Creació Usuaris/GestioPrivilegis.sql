--1. 
psql -U admin -h 127.0.0.1 -d postgres
--2. 
CREATE USER airadmin PASSWORD 'llamps i trons'; -- creem l'usuari amb contrasenya
--DINS DE admin (postgres=>)
grant airadmin to admin;
\q
psql
--estem a postgres, no a admin, però segueix sortint com postgres=>.
CREATE DATABASE airports WITH OWNER airadmin;
ALTER ROLE airadmin WITH CREATEROLE;-- per crear db i roles i usuaris.
\q
psql -U airadmin -h 127.0.0.1 -d airports
--3.
--DINS DE AIRADMIN
create schema flights;
create schema aircrafts;
create schema passengers;
--4.
CREATE ROLE GroundControl INHERIT;
CREATE ROLE AirTrafficControl NOINHERIT;
CREATE ROLE TicketSeller NOINHERIT;
CREATE ROLE Manager INHERIT;

GRANT USAGE ON SCHEMA flights, aircrafts, passengers TO GroundControl;
GRANT USAGE ON SCHEMA aircrafts TO AirTrafficControl;
GRANT USAGE ON SCHEMA flights, aircrafts TO TicketSeller;
GRANT USAGE ON SCHEMA flights TO Manager;
GRANT CREATE, USAGE ON SCHEMA flights TO AirTrafficControl;
GRANT CREATE, USAGE ON SCHEMA passengers TO TicketSeller;
GRANT CREATE, USAGE ON SCHEMA aircrafts TO Manager;
--5.
CREATE ROLE maria LOGIN NOINHERIT PASSWORD 'maria';
CREATE ROLE pere LOGIN INHERIT PASSWORD 'pere';
CREATE ROLE pau LOGIN INHERIT PASSWORD 'pau';
CREATE ROLE anna LOGIN NOINHERIT PASSWORD 'anna';

GRANT Manager, AirTrafficControl TO maria;
GRANT GroundControl, TicketSeller TO pere;
GRANT AirTrafficControl TO pau;
GRANT GroundControl, Manager TO anna;

--6.
psql -U airadmin -h 127.0.0.1 -d airports --desde airadmin
airports=> GRANT ALL PRIVILEGES ON SCHEMA aircrafts TO maria;

psql -U maria -h 127.0.0.1 -d airports-- amb l'usuari maria
/*aixó ho he consultat de la pagina https://todopostgresql.com/estructura-de-datos-en-postgresql-create-drop/
per veure \d aircrafts.airlines
o per veure si estan totes les taules creades --\dt aircrafts.*--*/
--table 1
CREATE TABLE aircrafts.Airlines (Id serial PRIMARY KEY, ICAOCode char(3) UNIQUE, Name varchar(50), 
Country varchar(40), OtherDetails text);

--table 2
CREATE TABLE aircrafts.Manufacturers (Id serial PRIMARY KEY, ICAOCode varchar(50) UNIQUE, Name varchar(100), 
OtherDetails text);

--table 3
CREATE TABLE aircrafts.AircraftModels (Id serial PRIMARY KEY, Code varchar(10), ManufacturerId integer,
Name varchar(100), Capacity smallint, Weight int, OtherDetails text, 
constraint fk_AircraftModels_Manufacturers foreign key (ManufacturerId) references aircrafts.Manufacturers(Id));

--table 4
CREATE TABLE aircrafts.Aircrafts (Id serial PRIMARY KEY, AirlineId integer, RegistrationId varchar(10) U
NIQUE, ModelId integer, Name varchar(100), OtherDetails text, 
CONSTRAINT fk_Aircrafts_AirlineId FOREIGN KEY (AirlineId) REFERENCES aircrafts.Airlines(Id), 
CONSTRAINT fk_Aircrafts_ModelId FOREIGN KEY (ModelId) REFERENCES aircrafts.AircraftModels(Id));

--7.
--desde airadmin dono permisos per a tots els usuaris al schema flights
GRANT ALL PRIVILEGES ON SCHEMA flights TO maria, pere, pau, anna;
--a continuació li dono privilegis sobre l'esquema passengers a pere.
GRANT ALL PRIVILEGES ON SCHEMA passengers TO pere;

-- Creem les taules: 

----Taula 1:----
CREATE TABLE flights.Airports (Id serial PRIMARY KEY, AirportCode char(6), Terminal varchar(2), City var
char(100), CityCode char(3), Country varchar(100), CountryCode char(3));

----Taula 2:----
CREATE TABLE flights.ActualFlights (Id serial PRIMARY KEY, AircraftId integer, DepartureTime time, 
ArrivalTime time, FlightDuration interval hour to minute, 
CONSTRAINT fk_ActualFlights_Aircraft FOREIGN KEY (AircraftId) REFERENCES aircrafts.Aircrafts(Id));

----Taula 3:----
CREATE TABLE flights.FlightSchedules (Id serial PRIMARY KEY, FlightCode char(6), ActualFlightId integer,
AirlineId integer, Date date, DepartureTime time, ArrivalTime time, Origin integer, Destination integer, 
FlightDuration interval hour to minute, 
CONSTRAINT fk_FlightSchedule_ActualFlight FOREIGN KEY (ActualFlightId) REFERENCES flights.ActualFlights(Id), 
CONSTRAINT fk_FlightSchedules_Airlines FOREIGN KEY (AirlineId) REFERENCES aircrafts.Airlines(Id), 
CONSTRAINT fk_FlightSchedules_Origin FOREIGN KEY (Origin) REFERENCES flights.Airports(Id), 
CONSTRAINT fk_FlightSchedule_Destination FOREIGN KEY (Destination) REFERENCES flights.Airports(Id));

--8.

----Tabla 1:----
CREATE TABLE passengers.Passengers (Id serial PRIMARY KEY, FirstName varchar(50), LastName varchar(100), 
CountryCode char(3), DocumentNumber varchar(15), DocumentType varchar(40), Email varchar(100), 
PhoneNumer varchar(15), OtherDetails text);

----Tabla 2:----
--en airadmin - airports=>
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA flights TO pere;-- Permis per utilitzar l'esquema flights a pere.
-- amb usuari pere: 
CREATE TABLE passengers.Reservations (Id serial PRIMARY KEY, Code char(6) UNIQUE, FlightScheduleId int, 
CONSTRAINT fk_Reservations_FlightSchedule FOREIGN KEY (FlightScheduleId) REFERENCES flights.FlightSchedules(Id));

----Tabla 3:----
CREATE TABLE passengers.PassengerReservations (Id serial PRIMARY KEY, PassengerId int, ReservationId int, 
Seat varchar(5), 
CONSTRAINT fk_PassengerReservations_Passenger FOREIGN KEY (PassengerId) REFERENCES passengers.Passengers(Id), 
CONSTRAINT fk_PassengerReservations_Reservation FOREIGN KEY (ReservationId) REFERENCES passengers.Reservations(Id));

--9.
CREATE VIEW ReservationInfo AS 
    SELECT r.Code, count(PassengerId) as NumPassengers, fs.date, fs.FlightCode, fs.Origin, fs.Destination, fs.DepartureTime, fs.ArrivalTime 
    FROM PassengerReservations pr JOIN Reservations r on pr.ReservationId = r.Id 
    JOIN FlightSchedules fs on r.FlightScheduleId = fs.Id
    GROUP BY r.Code, fs.date, fs.FlightCode, fs.Origin, fs.Destination, fs.DepartureTime, fs.ArrivalTime

--10.
CREATE VIEW AircraftsView AS
    SELECT am.Name, m.Name, al.Name
    FROM Airlines al JOIN Aircrafts a on al.Id = a.AirlineId
    JOIN AircraftModels am on a.ModelId = am.Id
    JOIN Manufacturers m on am.ManufacturerId = m.Id